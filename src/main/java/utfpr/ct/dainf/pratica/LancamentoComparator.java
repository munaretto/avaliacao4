package utfpr.ct.dainf.pratica;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparable<Lancamento>{
    
    private Date data;
    // outros metodos e atributos
 
    @Override
    public int compareTo(Lancamento outroLan) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date data1 = new Date(this.data.getTime());
	Date data2 = new Date(outroLan.getData().getTime());
	if(data1.after(data2)){
		return 1;
	}else if(data1.before(data2)){
		return -1;
	}else{
		System.out.println("Data: " + this.data.getTime() + " é igual à " + outroLan.getData().getTime());
	}

        return 0;
    }
}
